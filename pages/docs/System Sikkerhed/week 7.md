# Week 7

## The role of the OS

The main purpose of any operating system (OS), is to abstract away the fundamental workings of a computer, to allow user facing apps to be developed with minimum amount of code lines. In relation to IT Security, is it an advantage have a common OS, so possible number vulnerabilities of the system, are kept lower.
